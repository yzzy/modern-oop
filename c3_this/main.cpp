#include <iostream>
using namespace std;

const double PI{3.1415926};

class Circle
{
public:
    static int number;
    // constructor
    Circle()
    {
        // 实例增加一
        number++;
        radius = 1.0;
        cout << "constructor default" << endl;
    }
    Circle(double radius)
    {
        number++;
        this->radius = radius;
    }
    // 析构
    ~Circle()
    {
        number--;
        cout << " destructor " << endl;
        // cout << Circle::number << endl;
    }
    // 面积
    double area()
    {
        return PI * radius * radius;
    }
    void print_object()
    {
        cout << "OBJECT address:" << this << endl;
        cout << "radius :" << this->radius << endl;
    }

    // 指针object->a()->b()
    Circle *set_radius_point(double radius)
    {
        cout << "set point" << endl;
        this->radius = radius;
        return this;
    }
    Circle *print_radius_point()
    {
        cout << "print : " << this->radius << endl;
        return this;
    }

    // 引用object.a().b().c()
    Circle &set_radius_ref(double radius)
    {
        cout << "set point" << endl;
        this->radius = radius;
        return *this; // *this 实例
    }
    Circle &print_radius_ref()
    {
        cout << "print : " << this->radius << endl;
        return *this;
    }

private:
    double radius{0};
};

// 初始化类属性
int Circle::number = 0;

int main(int argc, char *argv[])
{
    // 堆上
    Circle *c = new Circle(3);
    c->print_object();
    c->print_radius_point()->set_radius_point(4)->print_radius_point();
    cout << "-----" << endl;
    c->print_radius_ref().set_radius_ref(5).print_radius_ref();

    // 栈上 gdb
    Circle c1(3);
    c1.print_object();
    c1.print_radius_point()->set_radius_point(4)->print_radius_point();
    cout << "-----" << endl;
    c1.print_radius_ref().set_radius_ref(5).print_radius_ref();

    cout << "----- yz ------" << endl;
    return 0;
}