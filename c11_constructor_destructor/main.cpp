#include <iostream>
#include "product.h"
using namespace std;

int main(int argc, char *argv[])
{
    {
        Product p1("Box", "Toy");
        p1.print_object();
    }
    cout << "----------------" << endl;
    Product p1("Box");
    p1.print_object();
    cout << "----- yz ------" << endl;
    return 0;
}