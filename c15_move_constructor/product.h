#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>
#include <string>
class Product
{
public:
    Product(int i);
    // copy
    // Product(const Product p); bad
    Product(const Product &p);
    Product(Product &&p);

    ~Product();

    // setter
    void set_years(int years) { *(this->years) = years; }
    // getter
    int *get_years() const { return this->years; }

    // move years
    int *move_years()
    {
        int *new_years{years};
        years = nullptr;
        return new_years;
    }

    void print_object()
    {
        std::cout << "Product : " << this
                  << ", years: " << *(this->years)
                  << ", years address: " << this->years << std::endl;
    }

private:
    int *years{};
};

#endif
