#include "product.h"

Product::~Product()
{
    delete years;
}

Product::Product(int i) : years(new int(i))
{
}

// copy
// 浅拷贝
// Product::Product(const Product &p) : name(p.get_name()), category(p.get_category()), years(p.get_years())
// {
// }
// 深拷贝
Product::Product(const Product &p) : years(new int(*p.get_years()))
{
}
Product::Product(Product &&p) : years(p.move_years())
{
}