#include "circle.h"

// 初始化类属性
int Circle::number = 0;

void Circle::print_object()
{
    std::cout << "OBJECT address:" << this << std::endl;
    std::cout << "radius :" << this->get_radius() << std::endl;
}
// 指针object->a()->b()
Circle *Circle::set_radius_point(double radius)
{
    std::cout << "set point" << std::endl;
    this->radius = radius;
    return this;
}
Circle *Circle::print_radius_point()
{
    std::cout << "print : " << this->get_radius() << std::endl;
    return this;
}

// 引用object.a().b().c()
Circle &Circle::set_radius_ref(double radius)
{
    std::cout << "set point" << std::endl;
    this->radius = radius;
    return *this; // *this 实例
}
Circle &Circle::print_radius_ref()
{
    std::cout << "print : " << this->get_radius() << std::endl;
    return *this;
}