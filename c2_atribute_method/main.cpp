#include <iostream>
using namespace std;

const double PI{3.1415926};

class Circle
{
public:
    static int number;
    // constructor
    Circle()
    {
        // 实例增加一
        number++;
        radius = 1.0;
        cout << "constructor default" << endl;
    }
    Circle(double param_radius)
    {
        number++;
        radius = param_radius;
    }
    // 析构
    ~Circle()
    {
        number--;
        cout << " destructor " << endl;
        // cout << Circle::number << endl;
    }
    // 面积
    double area()
    {
        return PI * radius * radius;
    }

private:
    double radius{0};
};

// 初始化类属性
int Circle::number = 0;

int main(int argc, char *argv[])
{
    // 栈上的对象
    Circle circle(2);
    cout << "area:" << circle.area() << endl;

    // 堆上 自己删除
    Circle *c = new Circle(3);
    cout << circle.area() << endl;
    cout << Circle::number << endl;
    delete c;
    c = nullptr;

    cout << Circle::number << endl;
    cout << "----- yz ------" << endl;
    return 0;
}